#!/bin/bash

#TODO:
#	A check of whether arbor is installed is needed, since it cannot be installed using RepositoryRepository
#	Something better to avoid mirroring things we are not allowed to mirror
#	verbose should probably be a cmdline switch
#	switch on unofficial repos

# email message and log file [print message] and exit
die() {
	HOME="/tmp" LANG="en_GB.utf8" EMAIL="Exherbo Distfiles Mirror <mirrordaemon@distfiles.exherbo.org>" mutt -s "Warning! $1" -i ${2} -- ${MAILTO} < ${HOME}/mail-error.txt
	if [[ ${VERBOSE} == "yes" ]];then
		echo "$1"
	fi
	if [[ -n ${3} ]];then
		return
	else
		exit 1
	fi
}

logger() {
	if [[ ${VERBOSE} == "yes" ]];then
		echo "$@"
	fi
}

VERBOSE=${VERBOSE:-no}
MAILTO=${MAILTO:-mirror-failure@exherbo.org}
REPOS=${REPOS:-"${HOME}/repositories"}
DATE=$(date +%Y-%m-%d-%H%M)
LOGPATH=${LOGPATH:-"${HOME}/log/${DATE}"}
DOWNLOAD_DIR=${DOWNLOAD_DIR:-"/srv/www/htdocs/distfiles"}
CAVE=${CAVE:-"cave -E :exherbo"}

#log to a dir for each date
mkdir ${LOGPATH}
#avoid being root for some install operations
export PALUDIS_NO_LIVE_DESTINATION=iamcheating
#skip hooks that need root
export PALUDIS_IGNORE_HOOKS_IN="update_config_protect_list.bash"

logger "Syncing all repositories"
${CAVE} sync &> "${LOGPATH}"/full-sync-report.log || die "Syncing all repositories failed" "${LOGPATH}"/full-sync-report.log

#run cave mirror for all package IDs we can fetch
logger "running cave mirror for all IDs"
LOG="${LOGPATH}/cave-mirror.log"
FAILED_IDS="${LOGPATH}/failed-ids.log"
echo "The following IDs have failed:" > ${FAILED_IDS}
ERRORS="${LOGPATH}/errors.log"
echo "----------------------------------------------" > ${ERRORS}
(( FAILED=0 ))

for ID in $(
	${CAVE} print-ids -s fetch -f '%c/%p%:%s::%r[=%v]\n' |
		sed -e '\#net-www/chromium-dev-pdf-plugin:0::desktop#d'
)
do
	logger "Running 'cave mirror' for ID: ${ID}"
	OUT=$(${CAVE} mirror -m ${ID} 2>&1)
	if [[ $? -ne 0 ]] ; then
		(( FAILED+=1 ))
		echo ${ID} >> ${FAILED_IDS}
		echo "${OUT}" >> ${ERRORS}
		logger "${OUT}"
	else
		echo "${OUT}" &>> ${LOG}
		logger "${OUT}"
	fi
done

#FIXME: hack to remove files we may not mirror
rm -f ${DOWNLOAD_DIR}/*flash*
rm -f ${DOWNLOAD_DIR}/*NVIDIA*
rm -f ${DOWNLOAD_DIR}/*nerolinux*
rm -f ${DOWNLOAD_DIR}/*VirtualBox*
rm -f ${DOWNLOAD_DIR}/*google-chrome*
rm -f ${DOWNLOAD_DIR}/emacs-23.1.9*
rm -f ${DOWNLOAD_DIR}/emacs-23.[23].*
rm -f ${DOWNLOAD_DIR}/emacs-23.3-rc*
rm -f ${DOWNLOAD_DIR}/skype*

#clean up
rm -rf ${DOWNLOAD_DIR}/*PARTIAL-

if [[ ${FAILED} -ne 0 ]] ; then
	cat ${FAILED_IDS} ${ERRORS} > "${LOGPATH}/error-mail.txt"
	die "${FAILED} IDs failed to fetch" "${LOGPATH}/error-mail.txt"
fi

logger "Sending completion mail"
HOME="/tmp" LANG="en_GB.utf8" EMAIL="Exherbo Distfiles Mirror <mirrordaemon@distfiles.exherbo.org>" mutt -s "Completed daily run" -- ${MAILTO} < ${HOME}/mail-completed.txt

