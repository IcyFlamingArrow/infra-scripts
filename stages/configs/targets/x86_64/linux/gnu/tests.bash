CPN_SKIP_TESTS+=(
    # Fails a test due to a sandbox violation
    # Last checked: 05 Mar 2019 (version: 3.6.6)
    # Context:
    #
    # sydbox: 8< -- Access Violation! --
    # sydbox: bind(-1, inet:0.0.0.0@50107)
    # sydbox: proc: lt-gnutls-serv[28004] (parent:27986)
    # sydbox: cwd: `/var/tmp/paludis/build/dev-libs-gnutls-3.6.6/work/gnutls-3.6.6/tests'
    # sydbox: cmdline: `/var/tmp/paludis/build/dev-libs-gnutls-3.6.6/work/gnutls-3.6.6/src/.libs/lt-gnu'
    # sydbox: >8 --
    # FAIL: gnutls-cli-invalid-crl.sh
    dev-libs/gnutls
)
