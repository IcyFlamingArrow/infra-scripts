#! /usr/bin/ruby

# Copyright (c) 2008 David Leverton <levertond@googlemail.com>
# Based in part upon 'playman.rb', which is:
#     Copyright (c) 2007 Mike Kelly <pioto@pioto.org>
#
# This program is is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License, version
# 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA

require 'Paludis'

require 'getoptlong'
require 'fileutils'

require 'rexml/document'
require 'uri'
require 'net/http'

def die(msg)
    $stderr.puts "#$0: #{msg}"
    exit 1
end
def warn(msg)
    $stderr.puts "#$0: #{msg}"
end

def unlink_if_exists(file)
    File.unlink(file) if File.exists?(file)
end
def rmdir_if_exists(dir)
    FileUtils.rm_r(dir) if File.exists?(dir)
end

module Enumerable
    def group_by
        res   = []
        prev  = nil
        each do | item |
            curr = yield item
            res << [] if res.empty? || curr != prev
            res.last << item
            prev = curr
        end
        res
    end
end

class String
    def normalise_space
        strip.tr("\t\n", " ").squeeze(" ")
    end
end

def infer_homepage(sync)
    case sync
    when %r{^git://git\.overlays\.gentoo\.org/(.+\.git)$}
        "http://git.overlays.gentoo.org/gitweb/?p=#$1"
    when %r{^svn://overlays\.gentoo\.org/([^/]+/[^/]+)(?:/.*)?$}
        "http://overlays.gentoo.org/#$1/"
    when %r{^svn\+https?://overlays\.gentoo\.org/svn/([^/]+/[^/]+)(?:/.*)?$}
        "http://overlays.gentoo.org/#$1/"
    when %r{^git://repo\.or\.cz/(.+\.git)$}
        "http://repo.or.cz/w/#$1"
    when %r{^git://github\.com/(.+)\.git$}
        "http://github.com/#$1"
    when %r{^git://gitorious.org/(.+)/(.+)\.git$}
        "http://www.gitorious.org/projects/#$1/repos/#$2"
    when %r{^git\+https://git\.exherbo\.org/git/(.+\.git)?$}
        "https://git.exherbo.org/#$1/"
    else ""
    end
end

class Distribution
    class Gentoo < Distribution
        LAYMAN_GLOBAL = URI.parse('https://api.gentoo.org/overlays/repositories.xml')

        def initialize(params)
            @blacklist  = params[:blacklist]
            @homepages  = params[:homepages]
            @output_dir = params[:output_dir]
            @layman     = params[:state] ? eval(params[:state]) : {}
            warn "--exclude is ignored for Gentoo" unless params[:exclude].empty?
        end

        def format
            return "2"
        end

        None = ""; class <<None; def text; self end end
        def update_repositories(env)
            die "only 'paludis' environment format is supported" unless env.format_key.parse_value == "paludis"

            begin
                req = Net::HTTP::Get.new(LAYMAN_GLOBAL.path)
                res = Net::HTTP.start(LAYMAN_GLOBAL.host, LAYMAN_GLOBAL.port, nil, nil, nil, nil, { :use_ssl => true }) do | http |
                    http.request(req)
                end
                xml = REXML::Document.new(res.body)
            rescue Exception
                die "couldn't open repositories.xml: #$!"
            end

            xml.elements.each("repositories/repo") do | overlay |
                name = overlay.elements["name"].text
                next if name == "gentoo"
                if @blacklist[name] then
                    @blacklist.delete(name)
                    next
                end
                if name =~ /[^-a-zA-Z0-9_]/ then
                    warn "repository name '#{name}' is not valid"
                    next
                end

                link        = (overlay.elements["homepage"]    || None).text.normalise_space
                description = (overlay.elements["description"] || None).text.normalise_space

                sources = []
                overlay.elements.each("source") do | source |
                    type        = source.attribute("type").to_s
                    src         = source.text.strip

                    case type

                    when 'bzr'
                        case src
                        when %r{^bzr(?:\+ssh)?://}
                            sources << src
                        when %r{^[a-z+]+://}
                            sources << "bzr+#{src}"
                        when %r{^lp:(.*)}
                            sources << "bzr+lp://#$1"
                        else
                            sources << "bzr+file://#{src}"
                        end

                    when 'cvs'
                        case src
                        when /^:ext:(.*)$/
                            sources << "cvs+ext://#$1:#{subpath}"
                        when /^:pserver:(.*)$/
                            sources << "cvs+pserver://#$1:#{subpath}"
                        end

                    when 'darcs'
                        case src
                        when %r{^[a-z+]+://}
                            sources << "darcs+#{src}"
                        when /..:/
                            sources << "darcs+ssh://#{src}"
                        else
                            sources << "darcs+file://#{src}"
                        end

                    when 'git'
                        case src
                        when %r{^git(?:\+ssh)?://}
                            sources << src
                        when %r{^[a-z+]+://}
                            sources << "git+#{src}"
                        else
                            sources << "git+file://#{src}"
                        end

                    when 'mercurial'
                        sources << "hg+#{src}"

                    when 'rsync'
                        case src
                        when %r{^rsync://}
                            sources << src
                        when /..:/
                            sources << "rsync+ssh://#{src}"
                        else
                            sources << "file://#{src}"
                        end

                    when 'svn'
                        case src
                        when %r{^svn(?:\+ssh)?://}
                            sources << src
                        when %r{^[a-z+]+://}
                            sources << "svn+#{src}"
                        else
                            sources << "svn+file://#{src}"
                        end

                    when 'tar'
                        sources << "tar+#{src}"

                    end
                end

                if sources.empty? then
                    warn "can't figure out how to sync '#{name}'"
                    next
                end

                if link.empty? then
                    link = @homepages[name]
                else
                    warn "'#{name}' already has a homepage"
                end if @homepages.has_key?(name)

                link = infer_homepage(sources[0]) if link.empty?

                warn "can't figure out homepage for '#{name}'" if link.empty?
                @layman[name] = { :sync => sources.join(" "), :homepage => link, :description => description }
            end

            @blacklist.each_key do | name |
                warn "'#{name}' is blacklisted but does not exist"
            end

            repos = {}
            env.repositories do | repo |
                next unless repo.format_key.parse_value == "ebuild" or repo.format_key.parse_value == "e"
                next if self.exclude?(repo)
                repos[File.basename(repo.location_key.parse_value)] = repo
            end

            @layman.each_pair do | name, info |
                warn "adding overlay '#{name}'" unless repos.has_key?(name)
                if repos.has_key?(name) && repos[name]['sync'].parse_value[''] != info[:sync] then
                    warn "changing sync URL for '#{name}' from '#{repos[name]['sync'].parse_value['']}' to '#{info[:sync]}'"
                    rmdir_if_exists(repos[name].location_key.parse_value)
                    repos.delete(name)
                end

                if !repos.has_key?(name) then
                    File.open("#{env.config_location_key.parse_value}/repositories/#{name}.conf", "w") do | file |
                        file.print <<-EOF.gsub(/^ */, "")
                            format = e
                            master_repository_if_unknown = gentoo
                            location = ${my_repos}/#{name}
                            sync = #{info[:sync]}
                        EOF
                    end
                else
                    repos.delete(name)
                end
            end

            repos.each_pair do | name, repo |
                warn "deleting overlay '#{name}'"
                rmdir_if_exists(repo.location_key.parse_value)
                unlink_if_exists("#{env.config_location_key.parse_value}/repositories/#{name}.conf")
                unlink_if_exists("#{@output_dir}/#{name}.repository")
            end
        end

        def exclude?(repo)
            return ["gentoo", "virtuals"].include?(repo.name)
        end

        def homepage(repo)
            name = File.basename(repo.location_key.parse_value)
            @layman[name][:homepage]
        end

        def sync(repo)
            key = repo['sync']
            key.parse_value[''] if key && key.parse_value
        end

        def dependencies(repo)
            key = repo["master_repository"]
            key.parse_value.map { | a | "repository/#{a}" }.join(", ") if key && key.parse_value
        end

        def description(repo)
            name = File.basename(repo.location_key.parse_value)
            note = "[(p)layman name: #{name}] " if name != repo.name
            "#{note}#{@layman[name][:description]}"
        end

        def state
            @layman.inspect
        end
    end

    class Exherbo < Distribution
        def initialize(params)
            @exclude   = params[:exclude]
            @homepages = params[:homepages]
            warn "--blacklist is ignored for Exherbo" unless params[:blacklist].empty?
        end

        def format
            return "2"
        end

        def update_repositories(env)
            # nothing; manual configuration for Exherbo
        end

        def exclude?(repo)
            return repo.name == "arbor" || @exclude[repo.name]
        end

        def homepage(repo)
            key = repo["homepage"]
            if key && key.parse_value then
                warn "'#{repo.name}' already has a homepage" if @homepages[repo.name]
                return key.parse_value
            end

            return @homepages[repo.name] if @homepages[repo.name]
            homepage = infer_homepage(repo['sync'].parse_value[''])
            warn "can't figure out homepage for '#{repo.name}'" if homepage.empty?
            homepage
        end

        def sync(repo)
            key = repo['sync']
            key.parse_value[''] if key && key.parse_value
        end

        def dependencies(repo)
            key = repo["master_repository"]
            key.parse_value.map { | a | "repository/#{a}" }.join(", ") if key && key.parse_value
        end

        def description(repo)
            key = repo["summary"]
            key.parse_value if key && key.parse_value
        end

        def state
            ""
        end
    end
end

Paludis::Log.instance.program_name = $0

envspec      = ""
output_dir   = nil
blacklist    = {}
exclude      = {}
homepages    = {}
start_from   = nil
distro_state = nil

GetoptLong.new(
    [ '--log-level',         GetoptLong::REQUIRED_ARGUMENT ],
    [ '--environment', '-E', GetoptLong::REQUIRED_ARGUMENT ],
    [ '--output-dir',        GetoptLong::REQUIRED_ARGUMENT ],
    [ '--blacklist',         GetoptLong::REQUIRED_ARGUMENT ],
    [ '--exclude',           GetoptLong::REQUIRED_ARGUMENT ],
    [ '--homepage',          GetoptLong::REQUIRED_ARGUMENT ],
    [ '--start-from',        GetoptLong::REQUIRED_ARGUMENT ],
    [ '--distro-state',      GetoptLong::REQUIRED_ARGUMENT ]
).each do | opt, arg |
    case opt

    when '--log-level'
        Paludis::Log.instance.log_level = case arg
            when 'silent'  then  Paludis::LogLevel::Silent
            when 'warning' then  Paludis::LogLevel::Warning
            when 'qa'      then  Paludis::LogLevel::Qa
            when 'debug'   then  Paludis::LogLevel::Debug
            else die "invalid #{opt}: #{arg}"
        end

    when '--environment'
        envspec = arg

    when '--output-dir'
        output_dir = arg

    when '--blacklist'
        blacklist[arg] = true

    when '--exclude'
        exclude[arg] = true

    when '--homepage'
        arg =~ /^([^=]+)=(.+)$/
        homepages[$1] = $2

    when '--start-from'
        start_from = arg

    when '--distro-state'
        distro_state = arg

    end
end

die "must specify --output-dir" if output_dir.nil?
die "#{output_dir} does not exist or is not a directory" unless File.directory?(output_dir)
die "#{output_dir} is not writable" unless File.writable?(output_dir)

env = Paludis::EnvironmentFactory.instance.create(envspec)
begin
    distro = Distribution.const_get(env.distribution.capitalize).
        new(:blacklist => blacklist, :exclude => exclude, :homepages => homepages, :output_dir => output_dir, :state => distro_state)
rescue NameError
    die "unsupported distribution '#{env.distribution}'"
end

if start_from.nil? then
    distro.update_repositories(env)
    system("cave", "--environment", envspec,
           "--log-level", case Paludis::Log.instance.log_level
               when Paludis::LogLevel::Silent   then  'silent'
               when Paludis::LogLevel::Warning  then  'warning'
               when Paludis::LogLevel::Qa       then  'qa'
               when Paludis::LogLevel::Debug    then  'debug'
           end, "sync")
    env = Paludis::EnvironmentFactory.instance.create(envspec)
end

done_one = false
None = "(none)"; class <<None; def parse_value() self; end; end
env.repositories do | repo |
    next unless repo.some_ids_might_support_action(Paludis::SupportsActionTest.new(Paludis::InstallAction))
    next if distro.exclude?(repo)

    next if !start_from.nil? && repo.name != start_from && !done_one
    if done_one then
        args = ["ruby", $0,
                "--log-level", case Paludis::Log.instance.log_level
                    when Paludis::LogLevel::Silent   then  'silent'
                    when Paludis::LogLevel::Warning  then  'warning'
                    when Paludis::LogLevel::Qa       then  'qa'
                    when Paludis::LogLevel::Debug    then  'debug'
                end,
                "--environment", envspec,
                "--output-dir" , output_dir,
                blacklist.keys.collect { |b| ["--blacklist", b] },
                exclude  .keys.collect { |e| ["--exclude"  , e] },
                homepages.entries.collect { |r,h| ["--homepage" , r+"="+h] },
                "--start-from"  , repo.name,
                "--distro-state", distro.state].flatten
        exec(*args)
    end
    done_one = true

    name = File.basename(repo.location_key.parse_value)
    filename = "#{output_dir}/#{name}.repository"
    puts "Writing #{filename}"
    begin
        File.open(filename, "w") do | file |
            if distro.format == "2" then
                file.print <<-EOF.gsub(/^ */, "")
                    format = unavailable-2
                    repo_format = e
                    sync = #{distro.sync(repo)}
                    dependencies = #{distro.dependencies(repo)}
                    repo_name = #{repo.name}
                    homepage = #{distro.homepage(repo)}
                    description = #{distro.description(repo)}

                EOF
            else
                file.print <<-EOF.gsub(/^ */, "")
                    format = unavailable-1
                    repo_name = #{repo.name}
                    homepage = #{distro.homepage(repo)}
                    description = #{distro.description(repo)}

                EOF
            end

            repo.category_names do | cat |
                first_pkg_in_cat = true
                puts "  #{cat}/" if not repo.package_names(cat).empty?
                repo.package_names(cat) do | qpn |
                    slots = env[Paludis::Selection::AllVersionsGroupedBySlot.new(
                                    Paludis::Generator::Package.new(qpn) &
                                    Paludis::Generator::InRepository.new(repo.name) |
                                    Paludis::Filter::SupportsAction.new(Paludis::InstallAction))].
                                group_by { |id| id.slot_key.parse_value }
                    first_slot_in_pkg = true

                    slots.each do | slot |
                        if first_pkg_in_cat then
                            first_pkg_in_cat = false
                            file.puts "#{cat}/"
                        end
                        if first_slot_in_pkg then
                            first_slot_in_pkg = false
                            file.puts "    #{qpn.package}/"
                        end

                        file.print "        :#{slot.last.slot_key.parse_value}"
                        slot.each { | id | file.print " #{id.version}" }
                        desc = "[no description available]"
                        slot.reverse_each do | id |
                            if !id.short_description_key.nil? &&
                                    !id.short_description_key.parse_value.empty? then
                                desc = id.short_description_key.parse_value
                                break
                            end
                        end
                        file.puts " ; #{desc}"
                    end
                end
            end
        end

    rescue
        warn "got #$! while writing #{filename}, deleting"
        unlink_if_exists(filename)
    end
end

# vim: set sw=4 ts=4 tw=100 et :
